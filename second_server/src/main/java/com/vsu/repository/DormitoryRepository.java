package com.vsu.repository;

import com.vsu.entity.Dormitory;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DormitoryRepository extends MongoRepository<Dormitory, String> {}

package com.vsu.repository;

import com.vsu.entity.Student;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends MongoRepository<Student, String> {

    public Student findByStudentID(final String studentID);

}

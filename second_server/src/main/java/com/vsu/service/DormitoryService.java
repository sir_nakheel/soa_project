package com.vsu.service;

import java.util.List;

import com.vsu.entity.Dormitory;
import com.vsu.repository.DormitoryRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class DormitoryService {

    private DormitoryRepository dormitoryRepo;

    public List<Dormitory> getAll() {
        return dormitoryRepo.findAll();
    }

    public Dormitory getById(final String id) {
        return dormitoryRepo.findById(id).orElse(null);
    }

    public String save(final Dormitory dormitory) {
        return dormitoryRepo.save(dormitory).getId();
    }

    public void deleteById(final String id) {
        dormitoryRepo.deleteById(id);
    }

}

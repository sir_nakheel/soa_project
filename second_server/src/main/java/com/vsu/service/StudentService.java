package com.vsu.service;

import java.util.List;
import java.util.Random;

import com.vsu.entity.Enrollee;
import com.vsu.entity.Student;
import com.vsu.repository.StudentRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StudentService {

    private static Random random = new Random();

    private StudentRepository studentRepo;

    public List<Student> getAll() {
        return studentRepo.findAll();
    }

    public Student getById(final String id) {
        return studentRepo.findById(id).orElse(null);
    }

    public String save(final Student student) {
        return studentRepo.save(student).getId();
    }

    public void enrolle(final List<Enrollee> enrolled) {
        enrolled.forEach(enrollee -> {
            Student student = new Student();
            student.setFirstName(enrollee.getFirstName());
            student.setLastName(enrollee.getLastName());
            String studentID;
            do {
                studentID = String.format("%08d", Math.abs(random.nextInt()));
            } while (studentRepo.findByStudentID(studentID) != null);
            student.setStudentID(studentID);
            studentRepo.save(student);
        });
    }

    public void deleteById(final String id) {
        studentRepo.deleteById(id);
    }

}

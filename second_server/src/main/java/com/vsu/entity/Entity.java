package com.vsu.entity;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data public class Entity {

    @Id
    protected String id;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Entity && ((Entity) obj).getId().compareTo(id) == 0;
    }

}

package com.vsu.entity;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data public class Specialty extends Entity {

    @NotBlank(message = "{name.not-blank}")
    protected String name;

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str
            .append("{ id: ").append(id)
            .append(", name: ").append(name)
            .append(" }");
        return str.toString();
    }

}

package com.vsu.entity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data public class Enrollee extends Entity {

    @NotBlank(message = "{first-name.not-blank}")
    protected String firstName;

    @NotBlank(message = "{last-name.not-blank}")
    protected String lastName;

    @NotBlank(message = "{passport-id.not-blank}")
    protected String passportID;

    @NotNull(message = "{specialty.not-null}")
    protected Specialty specialty;

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str
            .append("{ id: ").append(id)
            .append(", firstName: ").append(firstName)
            .append(", lastName: ").append(lastName)
            .append(", passportID: ").append(passportID)
            .append(", specialty").append(specialty)
            .append(" }");
        return str.toString();
    }

}

package com.vsu.entity;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data public class Student extends Entity {

    @NotBlank(message = "{first-name.not-blank}")
    protected String firstName;

    @NotBlank(message = "{last-name.not-blank}")
    protected String lastName;

    @NotBlank(message = "{student-id.not-blank}")
    protected String studentID;

    protected Dormitory dormitory;

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str
            .append("{ id: ").append(id)
            .append(", firstName: ").append(firstName)
            .append(", lastName: ").append(lastName)
            .append(", studentID: ").append(studentID)
            .append(", dormitory: ").append(dormitory)
            .append(" }");
        return str.toString();
    }

}

package com.vsu.entity;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data public class Dormitory extends Entity {

    @NotNull(message = "{number.not-null}")
    protected Integer number;

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str
            .append("{ id: ").append(id)
            .append(", number: ").append(number)
            .append(" }");
        return str.toString();
    }

}

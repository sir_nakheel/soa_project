package com.vsu.controller;

import java.util.List;

import javax.validation.Valid;

import com.vsu.entity.Enrollee;
import com.vsu.entity.Student;
import com.vsu.service.StudentService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/students")
@AllArgsConstructor
public class StudentController {

    private StudentService studentService;

    @GetMapping
    public List<Student> getAll() {
        return studentService.getAll();
    }

    @GetMapping("/{id}")
    public Student getById(@PathVariable final String id) {
        return studentService.getById(id);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public String save(@RequestBody @Valid final Student student) {
        return studentService.save(student);
    }

    @PostMapping("/enrolle")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void save(@RequestBody final List<Enrollee> enrolled) {
        studentService.enrolle(enrolled);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable final String id) {
        studentService.deleteById(id);
    }

}

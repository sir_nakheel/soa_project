package com.vsu.controller;

import java.util.List;

import javax.validation.Valid;

import com.vsu.entity.Dormitory;
import com.vsu.service.DormitoryService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/dormitories")
@AllArgsConstructor
public class DormitoryController {

    private DormitoryService dormitoryService;

    @GetMapping
    public List<Dormitory> getAll() {
        return dormitoryService.getAll();
    }

    @GetMapping("/{id}")
    public Dormitory getById(@PathVariable final String id) {
        return dormitoryService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public String save(@RequestBody @Valid final Dormitory dormitory) {
        return dormitoryService.save(dormitory);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable final String id) {
        dormitoryService.deleteById(id);
    }

}

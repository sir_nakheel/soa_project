package com.vsu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/student.html")
    public String student() {
        return "student";
    }

    @GetMapping("/dormitories.html")
    public String dormitories() {
        return "dormitories";
    }

    @GetMapping("/dormitory.html")
    public String dormitory() {
        return "dormitory";
    }

}

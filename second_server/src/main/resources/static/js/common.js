function createTableOfStudents() {
    $.get('/students', function(data, status) {
        if (status == 'success') {
            let tbody = document.getElementById('tableOfStudents');
            while (tbody.hasChildNodes())
                tbody.removeChild(tbody.firstChild);
            data.forEach(student => {
                let tr = document.createElement('tr');
                let td;

                td = document.createElement('td');
                td.appendChild(document.createTextNode(student.firstName + ' ' + student.lastName));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(student.studentID));
                tr.appendChild(td);

                td = document.createElement('td');
                if (student.dormitory != null)
                    td.appendChild(document.createTextNode(student.dormitory.number));
                tr.appendChild(td);

                td = document.createElement('td');
                let a = document.createElement('a');
                a.href = '/student.html/?id=' + student.id;
                a.appendChild(document.createTextNode('Change'));
                a.classList.add('btn');
                a.classList.add('btn-secondary');
                td.appendChild(a);
                tr.appendChild(td);

                td = document.createElement('td');
                let button = document.createElement('button');
                button.appendChild(document.createTextNode('Delete'));
                button.classList.add('btn');
                button.classList.add('btn-secondary');
                button.addEventListener('click', function() {
                    $.ajax({
                        url: '/students/' + student.id,
                        type: 'DELETE',
                        success: function(res) {
                            createTableOfStudents();
                        }
                    });
                });
                td.appendChild(button);
                tr.appendChild(td);

                tbody.appendChild(tr);
            });
        }
    });
};

function loadStudent() {
    let href = window.location.href;
    if (href.indexOf('id') != -1) {
        let id = href.slice(href.indexOf('id') + 3);
        $.get('/students/' + id, function(data, status) {
            if (status == 'success' && data != null) {
                document.getElementById('id').value = data.id;
                document.getElementById('firstName').value = data.firstName;
                document.getElementById('lastName').value = data.lastName;
                document.getElementById('studentID').value = data.studentID;
            }
        });
    }
    $.get('/dormitories', function(data, status) {
        if (status == 'success') {
            let select = document.getElementById('dormitory');
            data.forEach(dormitory => {
                let option = document.createElement('option');
                option.appendChild(document.createTextNode(dormitory.number));
                option.value = dormitory.id;
                select.appendChild(option);
            });
        }
    });
};

function saveStudent() {
    let student = {
        'id': null,
        'firstName': null,
        'lastName': null,
        'studentID': null,
        'dormitory': {
            "id": null,
            "number": null
        }
    };
    let id = document.getElementById('id').value;
    if (id != null && id != '')
        student.id = id;
    student.firstName = document.getElementById('firstName').value;
    student.lastName = document.getElementById('lastName').value;
    student.studentID = document.getElementById('studentID').value;
    $.get('/dormitories/' + document.getElementById('dormitory').value, function (data, status) {
        if (status == 'success' && data != null) {
            student.dormitory.id = data.id;
            student.dormitory.number = data.number;
            $.ajax({
                url: '/students',
                type: 'POST',
                data: JSON.stringify(student),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(res) {
                    document.getElementById('students-link').click();
                }
            });
        }
    });
};

function createTableOfDormitories() {
    $.get('/dormitories', function(data, status) {
        if (status == 'success') {
            let tbody = document.getElementById('tableOfDormitories');
            while (tbody.hasChildNodes())
                tbody.removeChild(tbody.firstChild);
            data.forEach(dormitory => {
                let tr = document.createElement('tr');
                let td;

                td = document.createElement('td');
                td.appendChild(document.createTextNode(dormitory.number));
                tr.appendChild(td);

                td = document.createElement('td');
                let a = document.createElement('a');
                a.href = '/dormitory.html/?id=' + dormitory.id;
                a.appendChild(document.createTextNode('Change'));
                a.classList.add('btn');
                a.classList.add('btn-secondary');
                td.appendChild(a);
                tr.appendChild(td);

                td = document.createElement('td');
                let button = document.createElement('button');
                button.appendChild(document.createTextNode('Delete'));
                button.classList.add('btn');
                button.classList.add('btn-secondary');
                button.addEventListener('click', function() {
                    $.ajax({
                        url: '/dormitories/' + dormitory.id,
                        type: 'DELETE',
                        success: function(res) {
                            createTableOfDormitories();
                        }
                    });
                });
                td.appendChild(button);
                tr.appendChild(td);

                tbody.appendChild(tr);
            });
        }
    });
};

function loadDormitory() {
    let href = window.location.href;
    if (href.indexOf('id') != -1) {
        let id = href.slice(href.indexOf('id') + 3);
        $.get('/dormitories/' + id, function(data, status) {
            if (status == 'success' && data != null) {
                document.getElementById('id').value = data.id;
                document.getElementById('number').value = data.number;
            }
        });
    }
};

function saveDormitory() {
    let dormitory = {
        'id': null,
        'number': null
    };
    let id = document.getElementById('id').value;
    if (id != null && id != '')
        dormitory.id = id;
    dormitory.number = document.getElementById('number').value;
    $.ajax({
        url: '/dormitories',
        type: 'POST',
        data: JSON.stringify(dormitory),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(res) {
            document.getElementById('dormitories-link').click();
        }
    });
};

function createListOfPlaces() {
    $.get('http://localhost:8080/specialties', function(data, status) {
        if (status == 'success') {
            var totalPlaces = 0;
            data.forEach(specialty => {
                let ul = document.getElementById('places');
                $.get('http://localhost:8080/listOfEnrolled/' + specialty.id, function(data, status) {
                    if (status == 'success') {
                        let li = document.createElement('li');
                        li.appendChild(document.createTextNode(specialty.name + ': ' + data.length));
                        li.classList.add('list-group-item');
                        ul.appendChild(li);
                        totalPlaces += data.length;
                        li = document.getElementById('totalPlaces');
                        while (li.hasChildNodes())
                            li.removeChild(li.firstChild);
                        li.appendChild(document.createTextNode('Total: ' + totalPlaces));
                    }
                });
            });
        }
    });
};

function getEnrolled() {
    $.get('http://localhost:8080/enrolle', function (data, status) {
        if (status == 'success') {
            $.ajax({
                url: '/students/enrolle',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function(res) {
                    createTableOfStudents();
                }
            });
        }
    });
};

function createSpecialtyIDSelect() {
    $.get('/specialties', function(data, status) {
        if (status == 'success') {
            let select = document.getElementById('specialtyID');
            data.forEach(specialty => {
                let option = document.createElement('option');
                option.appendChild(document.createTextNode(specialty.name));
                option.value = specialty.id;
                select.appendChild(option);
            });
        }
    });
};

function createTableOfEnrolled() {
    $.get('/listOfEnrolled/' + $('#specialtyID').val(), function(data, status) {
        if (status == 'success') {
            let tbody = document.getElementById('tableOfEnrolled');
            while (tbody.hasChildNodes())
                tbody.removeChild(tbody.firstChild);
            data.forEach(enrollee => {
                let tr = document.createElement('tr');
                let td;

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.firstName + ' ' + enrollee.lastName));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.passportID));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.totalMark));
                tr.appendChild(td);

                tbody.appendChild(tr);
            });
        }
    });
};

function createTableOfSpecialty() {
    $.get('/specialties', function(data, status) {
        if (status == 'success') {
            let tbody = document.getElementById('tableOfSpecialty');
            while (tbody.hasChildNodes())
                tbody.removeChild(tbody.firstChild);
            data.forEach(specialty => {
                let tr = document.createElement('tr');
                let td;

                td = document.createElement('td');
                td.appendChild(document.createTextNode(specialty.name));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(specialty.plane));
                tr.appendChild(td);

                td = document.createElement('td');
                let a = document.createElement('a');
                a.href = '/specialty.html?id=' + specialty.id;
                a.appendChild(document.createTextNode('Change'));
                a.classList.add('btn');
                a.classList.add('btn-secondary');
                td.appendChild(a);
                tr.appendChild(td);

                td = document.createElement('td');
                let button = document.createElement('button');
                button.appendChild(document.createTextNode('Delete'));
                button.classList.add('btn');
                button.classList.add('btn-secondary');
                button.addEventListener('click', function() {
                    $.ajax({
                        url: '/specialties/' + specialty.id,
                        type: 'DELETE',
                        success: function(res) {
                            createTableOfSpecialty();
                        }
                    });
                });
                td.appendChild(button);
                tr.appendChild(td);

                tbody.appendChild(tr);
            });
        }
    });
};

function loadSpecialty() {
    let href = window.location.href;
    if (href.indexOf('id') != -1) {
        let id = href.slice(href.indexOf('id') + 3);
        $.get('/specialties/' + id, function(data, status) {
            if (status == 'success' && data != null) {
                document.getElementById('id').value = data.id;
                document.getElementById('name').value = data.name;
                document.getElementById('plane').value = data.plane;
            }
        });
    }
};

function saveSpecialty() {
    var specialty = {
        "id": null,
        "name": null,
        "plane": null
    };
    let href = window.location.href;
    if (document.getElementById('id').value != null)
        specialty.id = document.getElementById('id').value;
    specialty.name = document.getElementById('name').value;
    specialty.plane = document.getElementById('plane').value;
    $.ajax({
        url: '/specialties',
        type: 'POST',
        data: JSON.stringify(specialty),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(res) {
            document.getElementById('specialties').click();
        }
    });
};

function createTableOfEnrollees() {
    $.get('/enrollees', function(data, status) {
        if (status == 'success') {
            let tbody = document.getElementById('tableOfEnrollees');
            while (tbody.hasChildNodes())
                tbody.removeChild(tbody.firstChild);
            data.forEach(enrollee => {
                let tr = document.createElement('tr');
                let td;

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.firstName + ' ' + enrollee.lastName));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.passportID));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.totalMark));
                tr.appendChild(td);

                td = document.createElement('td');
                td.appendChild(document.createTextNode(enrollee.specialty.name));
                tr.appendChild(td);

                td = document.createElement('td');
                let a = document.createElement('a');
                a.href = '/enrollee.html?id=' + enrollee.id;
                a.appendChild(document.createTextNode('Change'));
                a.classList.add('btn');
                a.classList.add('btn-secondary');
                td.appendChild(a);
                tr.appendChild(td);

                td = document.createElement('td');
                let button = document.createElement('button');
                button.appendChild(document.createTextNode('Delete'));
                button.classList.add('btn');
                button.classList.add('btn-secondary');
                button.addEventListener('click', function() {
                    $.ajax({
                        url: '/enrollees/' + enrollee.id,
                        type: 'DELETE',
                        success: function(res) {
                            createTableOfEnrollees();
                        }
                    });
                });
                td.appendChild(button);
                tr.appendChild(td);

                tbody.appendChild(tr);
            });
        }
    });
};

function loadEnrollee() {
    let href = window.location.href;
    if (href.indexOf('id') != -1) {
        let id = href.slice(href.indexOf('id') + 3);
        $.get('/enrollees/' + id, function(data, status) {
            if (status == 'success' && data != null) {
                document.getElementById('id').value = data.id;
                document.getElementById('firstName').value = data.firstName;
                document.getElementById('lastName').value = data.lastName;
                document.getElementById('passportID').value = data.passportID;
                document.getElementById('totalMark').value = data.totalMark;
            }
        });
    }
    $.get('/specialties', function(data, status) {
        if (status == 'success') {
            let select = document.getElementById('specialty');
            data.forEach(specialty => {
                let option = document.createElement('option');
                option.appendChild(document.createTextNode(specialty.name));
                option.value = specialty.id;
                select.appendChild(option);
            });
        }
    });
};

function saveEnrollee() {
    var enrollee = {
        'id': null,
        'firstName': null,
        'lastName': null,
        'passportID': null,
        'totalMark': null,
        'specialty': {
            'id': null
        }
    };
    if (document.getElementById('id').value != null)
        enrollee.id = document.getElementById('id').value;
    enrollee.firstName = document.getElementById('firstName').value;
    enrollee.lastName = document.getElementById('lastName').value;
    enrollee.passportID = document.getElementById('passportID').value;
    enrollee.totalMark = document.getElementById('totalMark').value;
    enrollee.specialty.id = document.getElementById('specialty').value;
    $.ajax({
        url: '/enrollees',
        type: 'POST',
        data: JSON.stringify(enrollee),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(res) {
            document.getElementById('enrollees').click();
        }
    });
}

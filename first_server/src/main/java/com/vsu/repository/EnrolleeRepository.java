package com.vsu.repository;

import java.util.List;

import com.vsu.entity.Enrollee;
import com.vsu.entity.Specialty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnrolleeRepository extends JpaRepository<Enrollee, Long> {
    public List<Enrollee> findBySpecialty(final Specialty specialty);
}

package com.vsu.controller;

import java.util.List;

import javax.validation.Valid;

import com.vsu.entity.Specialty;
import com.vsu.service.SpecialtyService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/specialties")
@AllArgsConstructor
public class SpecialtyController {

    private SpecialtyService specialtyService;

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping
    public List<Specialty> getAll() {
        return specialtyService.getAll();
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping("/{id}")
    public Specialty getById(@PathVariable final Long id) {
        return specialtyService.getById(id);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Long save(@RequestBody @Valid final Specialty specialty) {
        return specialtyService.save(specialty);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable final Long id) {
        specialtyService.deleteById(id);
    }

}

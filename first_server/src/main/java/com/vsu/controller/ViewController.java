package com.vsu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/specialties.html")
    public String specialties() {
        return "specialties";
    }

    @GetMapping("/specialty.html")
    public String specialty() {
        return "specialty";
    }

    @GetMapping("/enrollees.html")
    public String enrollees() {
        return "enrollees";
    }

    @GetMapping("/enrollee.html")
    public String enrollee() {
        return "enrollee";
    }

}

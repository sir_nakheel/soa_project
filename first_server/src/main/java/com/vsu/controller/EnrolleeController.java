package com.vsu.controller;

import java.util.List;

import javax.validation.Valid;

import com.vsu.entity.Enrollee;
import com.vsu.service.EnrolleeService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/enrollees")
@AllArgsConstructor
public class EnrolleeController {

    private EnrolleeService enrolleeService;

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping
    public List<Enrollee> getAll() {
        return enrolleeService.getAll();
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping("/{id}")
    public Enrollee getById(@PathVariable final Long id) {
        return enrolleeService.getById(id);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Long save(@RequestBody @Valid final Enrollee enrollee) {
        return enrolleeService.save(enrollee);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable final Long id) {
        enrolleeService.deleteById(id);
    }

}

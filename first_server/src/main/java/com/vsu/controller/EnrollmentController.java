package com.vsu.controller;

import java.util.List;

import com.vsu.entity.Enrollee;
import com.vsu.service.EnrollmentService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class EnrollmentController {

    private EnrollmentService enrollmentService;

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping("/listOfEnrolled/{specialtyID}")
    public List<Enrollee> getListOfEnrolled(@PathVariable final Long specialtyID) {
        return enrollmentService.getListOfEnrolled(specialtyID);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping("/enrolle")
    public List<Enrollee> enrolle() {
        return enrollmentService.enrolle();
    }

}

package com.vsu.service;

import java.util.List;

import com.vsu.entity.Enrollee;
import com.vsu.repository.EnrolleeRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EnrolleeService {

    private EnrolleeRepository enrolleeRepo;

    public List<Enrollee> getAll() {
        return enrolleeRepo.findAll();
    }

    public Enrollee getById(final Long id) {
        return enrolleeRepo.findById(id).orElse(null);
    }

    public Long save(final Enrollee enrollee) {
        return enrolleeRepo.save(enrollee).getId();
    }

    public void deleteById(final Long id) {
        enrolleeRepo.deleteById(id);
    }

}

package com.vsu.service;

import java.util.List;

import com.vsu.entity.Specialty;
import com.vsu.repository.SpecialtyRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SpecialtyService {

    private SpecialtyRepository specialtyRepo;

    public List<Specialty> getAll() {
        return specialtyRepo.findAll();
    }

    public Specialty getById(final Long id) {
        return specialtyRepo.findById(id).orElse(null);
    }

    public Long save(final Specialty specialty) {
        return specialtyRepo.save(specialty).getId();
    }

    public void deleteById(final Long id) {
        specialtyRepo.deleteById(id);
    }

}

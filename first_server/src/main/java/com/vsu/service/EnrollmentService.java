package com.vsu.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.vsu.comparator.TotalMarkComparator;
import com.vsu.entity.Enrollee;
import com.vsu.entity.Specialty;
import com.vsu.repository.SpecialtyRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EnrollmentService {

    private SpecialtyRepository specialtyRepo;

    public List<Enrollee> getListOfEnrolled(final Long specialtyID) {
        Specialty specialty = specialtyRepo.findById(specialtyID).orElse(null);
        List<Enrollee> enrolled = specialty.getEnrollees();
        Collections.sort(enrolled, new TotalMarkComparator());
        if (enrolled.size() > specialty.getPlane())
            enrolled = enrolled.subList(0, specialty.getPlane());
        return enrolled;
    }

    public List<Enrollee> enrolle() {
        List<Enrollee> enrolled = new ArrayList<>();
        specialtyRepo.findAll().forEach(specialty -> {
            enrolled.addAll(getListOfEnrolled(specialty.getId()));
        });
        return enrolled;
    }

}

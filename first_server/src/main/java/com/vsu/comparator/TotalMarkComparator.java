package com.vsu.comparator;

import java.util.Comparator;

import com.vsu.entity.Enrollee;

public class TotalMarkComparator implements Comparator<Enrollee> {

    @Override
    public int compare(Enrollee o1, Enrollee o2) {
        return -o1.getTotalMark().compareTo(o2.getTotalMark());
    }

}

package com.vsu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Data public class Enrollee extends SQLEntity {

    @NotBlank(message = "{first-name.not-blank}")
    @Column(nullable = false)
    protected String firstName;

    @NotBlank(message = "{last-name.not-blank}")
    @Column(nullable = false)
    protected String lastName;

    @NotBlank(message = "{passport-id.not-blank}")
    @Column(nullable = false, unique = true)
    protected String passportID;

    @NotNull(message = "{total-mark.not-null}")
    @Column(nullable = false)
    protected Integer totalMark;

    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonIgnoreProperties({"enrollees"})
    protected Specialty specialty;

}

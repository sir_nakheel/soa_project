package com.vsu.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Data public class Specialty extends SQLEntity {

    @NotBlank(message = "{name.not-blank}")
    @Column(nullable = false, unique = true)
    protected String name;

    @NotNull(message = "{plane.not-null}")
    @Column(nullable = false)
    protected Integer plane;

    @OneToMany(mappedBy = "specialty")
    @JsonIgnoreProperties({"specialty"})
    protected List<Enrollee> enrollees;

}

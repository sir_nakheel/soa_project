package com.vsu.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@MappedSuperclass
@Data public class SQLEntity {

    @Id
    @GeneratedValue
    protected Long id;

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof SQLEntity && id == ((SQLEntity) obj).getId()) return true;
        return false;
    }

}
